var index = 0;
var code = 0;

document.getElementById("btn").addEventListener("click", () => {
  var data_in = document.getElementById("input").value;
  const char_array = data_in.split("");

  var unicode = char_array.map((char) => {
    console.log(char);

    code = char.charCodeAt(index);
    if (code >= 65 && code <= 90) {
      return code - 64;
    } else if (code >= 97 && code <= 123) {
      return code - 96;
    } else {
      return char;
    }
    return code;
  });
  console.log(unicode);
  document
    .querySelector("body")
    .insertAdjacentText("beforeend", unicode.join(""));
});
